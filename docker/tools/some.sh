#!/usr/bin/env bash

source .env
source .env.local

if [ -f docker-compose.${APP_ENV}.yaml ]
then
    docker-compose -f docker-compose.yaml -f docker-compose.${APP_ENV}.yaml ${1:-up -d --build}
else
    docker-compose -f docker-compose.yaml ${1:-up -d --build}
fi
