# Launch
```bash
#start
./docker/tools/some.sh

#stop
./docker/tools/some.sh down
```
#Environment
```
#.env

APP_ENV=prod
```
To override default env value for specific machine you should create .env.local file. Example:
```
#.env.local

APP_ENV=dev
```
