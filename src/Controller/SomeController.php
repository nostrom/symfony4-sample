<?php
/**
 * Created by PhpStorm.
 * User: nostrom
 * Date: 10/01/2019
 * Time: 13:33
 */

namespace App\Controller;


use Symfony\Component\HttpFoundation\JsonResponse;

class SomeController
{
    public function test()
    {

        return new JsonResponse([mt_rand(1, 999), getenv('APP_ENV'), 'tt']);
    }
}
