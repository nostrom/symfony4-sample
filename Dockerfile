FROM composer:latest as composer

WORKDIR '/build'

COPY composer.json composer.lock /build/

RUN /bin/sh /docker-entrypoint.sh install --no-scripts

FROM php:7.3.0-fpm-alpine3.8

RUN apk update && apk upgrade && \
    apk add --no-cache \
    curl \
    php7-curl \
    bash

COPY . /app
COPY --from=composer /build/vendor /app/vendor

WORKDIR '/app'
